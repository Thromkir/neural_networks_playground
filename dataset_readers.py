import os
import pickle
import numpy as np


def read_cifar_10(
        datasetDir="./datasets/cifar_10",
        interestedClasses=None
):
    with open(os.path.join(datasetDir, "batches.meta"), "rb") as fo:
        info = pickle.load(fo, encoding="bytes")
        LABEL_NAMES = info[b"label_names"]
    classesLabels = np.nonzero(np.isin(LABEL_NAMES, LABEL_NAMES)) if interestedClasses is None else np.nonzero(np.isin(LABEL_NAMES, interestedClasses))
    # deal with train data
    dataBatches = [
        "data_batch_1",
        "data_batch_2",
        "data_batch_3",
        "data_batch_4",
        "data_batch_5",
    ]
    trainData = np.empty((0, 32, 32, 3))
    trainLabels = np.empty(0)
    for dataBatchName in dataBatches:
        with open(os.path.join(datasetDir, dataBatchName), "rb") as fo:
            data = pickle.load(fo, encoding="bytes")
            interestedIndices = np.isin(data[b"labels"], classesLabels)
            interestedData = data[b"data"][interestedIndices]
            interestedLabels = np.array(data[b"labels"])[interestedIndices]

            interestedData = np.reshape(interestedData, (interestedData.shape[0], 3, 32, 32))
            interestedData = np.moveaxis(interestedData, 1, -1)
            trainData = np.concatenate((trainData, interestedData), axis=0)
            trainLabels = np.concatenate((trainLabels, interestedLabels), axis=0)

    # deal with test data
    with open(os.path.join(datasetDir, "test_batch"), "rb") as fo:
        data = pickle.load(fo, encoding="bytes")
        interestedIndices = np.isin(data[b"labels"], classesLabels)
        testData = data[b"data"][interestedIndices]
        testLabels = np.array(data[b"labels"])[interestedIndices]

        testData = np.reshape(testData, (testData.shape[0], 3, 32, 32))
        testData = np.moveaxis(testData, 1, -1)
    return (trainData, trainLabels), (testData, testLabels), LABEL_NAMES