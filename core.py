import os
import numpy as np
import cv2
import tensorflow as tf
from nets.VGG16 import VGG16
from dataset_readers import read_cifar_10
from normalization import simple_image_normalization
from sklearn.preprocessing import LabelBinarizer

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        # Currently, memory growth needs to be the same across GPUs
        for gpu in gpus:
            tf.config.experimental.set_memory_growth(gpu, True)
        logical_gpus = tf.config.experimental.list_logical_devices('GPU')
        print(len(gpus), "Physical GPUs,", len(logical_gpus), "Logical GPUs")
    except RuntimeError as e:
        # Memory growth must be set before GPUs have been initialized
        print(e)

CLASSES_TO_LEARN = [b"cat", b"dog"]
(trainData, trainLabels), (testData, testLabels), LABELS_NAMES = read_cifar_10(interestedClasses=CLASSES_TO_LEARN)
TRAIN_DATA_PART = 0.8
TRAIN_DATA_SIZE = int(trainData.shape[0] * TRAIN_DATA_PART)
VALIDATION_DATA_SIZE = trainData.shape[0] - TRAIN_DATA_SIZE
INPUT_HEIGHT = trainData.shape[1]
INPUT_WIDTH = trainData.shape[2]
INPUT_DEPTH = trainData.shape[3]
BATCH_SIZE = 64
NUM_EPOCHS = 200

lb = LabelBinarizer()
trainLabels = lb.fit_transform(trainLabels)
testLabels = lb.transform(testLabels)
trainData = simple_image_normalization(trainData)
testData = simple_image_normalization(testData)

trainDataset = tf.data.Dataset.from_tensor_slices((trainData, trainLabels)).shuffle(10000)
validationDataset = trainDataset.take(VALIDATION_DATA_SIZE).cache().batch(BATCH_SIZE)
trainDataset = trainDataset.skip(VALIDATION_DATA_SIZE).take(TRAIN_DATA_SIZE).cache().batch(BATCH_SIZE).repeat()
testDataset = tf.data.Dataset.from_tensor_slices((testData, testLabels)).batch(BATCH_SIZE)

modelLoss = tf.keras.losses.BinaryCrossentropy(from_logits=False)
modelOptimizer = tf.keras.optimizers.Adam()
model = VGG16.build(height=INPUT_HEIGHT, width=INPUT_WIDTH, depth=INPUT_DEPTH, numClasses=len(CLASSES_TO_LEARN))
model.compile(
    loss=modelLoss,
    optimizer=modelOptimizer,
    metrics=["accuracy", tf.keras.losses.BinaryCrossentropy(from_logits=False, name="binary_crossentropy")],
)
MODEL_NAME = "VGG16_adam(default)_batch64_2"
modelCallbacks = [
    tf.keras.callbacks.TensorBoard(log_dir=os.path.join("logs", MODEL_NAME), histogram_freq=1),
    tf.keras.callbacks.ModelCheckpoint(
        filepath=os.path.join("checkpoints", MODEL_NAME, "e-{epoch:04d}-acc-{val_accuracy:04f}-loss-{val_binary_crossentropy:04f}"),
        monitor="val_accuracy",
        verbose=0,
        save_best_only=True,
        mode="max"
    ),
    tf.keras.callbacks.EarlyStopping(monitor="val_binary_crossentropy", patience=20)
]

history = model.fit(
    trainDataset,
    validation_data=validationDataset,
    steps_per_epoch=TRAIN_DATA_SIZE // BATCH_SIZE,
    epochs=NUM_EPOCHS,
    callbacks=modelCallbacks,
    verbose=1
)